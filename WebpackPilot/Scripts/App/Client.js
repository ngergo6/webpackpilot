﻿import * as services from "./Services/index";

(() => {
    "use strict";

    // 3rd party dependencies
    let angular = require("angular");

    //load global modules
    var $ = window.jQuery = window.$ = require("../bower_components/jquery/dist/jquery.min");
    require("../bower_components/bootstrap/dist/js/bootstrap.min");

    // CommonJS require controllers
    let controllers = require("./Controllers");

    // create the app
    let app = angular.module("Application", []);

    // bootstrap the app
    let bootstrap = () => {
        angular.element(document).ready(() => {
            angular.bootstrap(document, ["Application"]);
        });
    };

    services.register(app);

    // register controllers
    controllers(app, bootstrap);
})();