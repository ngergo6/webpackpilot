﻿(function () {
    "use strict";

    //var _ = require("../../bower_components/underscore/underscore-min");
    //var controllerList = ["HomeController", "ItemDetailsController"];
    //var registerControllers = function (app) {
    //    _.each(controllerList, function (controllerName) {
    //        var controllerModule = require("./" + controllerName);
    //        controllerModule.register(app);
    //    });
    //};

    var registerControllers = function (app, callback) {

        require("./HomeController").register(app);
        require(["./ItemDetailsController"], function (ctrl) {
            ctrl.register(app, callback);
        });
    };

    module.exports = registerControllers;

})();