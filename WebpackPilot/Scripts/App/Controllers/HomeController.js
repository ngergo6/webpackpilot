﻿(function () {
    "use strict";

    var homeController = function($scope, $http, itemDetaisService) {

        $scope.dataItems = [];

        itemDetaisService.getItems(function(items) {
            $scope.dataItems.length = 0;
            $scope.dataItems = items.map((d) => {
                d.open = () => { $scope.$broadcast("open_item", d.id) };
                return d;
            });
        });

    };

    var register = function (app) {
        app.controller("HomeController", ["$scope", "$http", "ItemDetailsService", homeController]);
    }

    module.exports = {
        register: register
    };
})();