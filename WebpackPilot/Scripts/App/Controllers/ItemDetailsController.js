﻿(function () {
    "use strict";

    define("ItemDetailsController", [], function () {
        var register = function (app, callback) {

            app.controller("ItemDetailsController", ["$scope", "$http", "ItemDetailsService", function ($scope, $http, itemDetailsService) {

                $scope.id = "";
                $scope.name = "";
                $scope.description = "";
                $scope.modalTitle = () => `Details of ${$scope.name}`;

                $scope.$on("open_item", function (event, id) {
                    if (id) {
                        itemDetailsService.getItemById(id, function(object) {
                            if (!object) return;

                            $scope.id = object.id || "";
                            $scope.name = object.name || "";
                            $scope.description = object.description || "";

                            $("#details-modal").modal("show");
                        });
                    }
                });

            }]);

            if (callback) callback();
        }

        return { register: register };
    });
})();