﻿var register = undefined;

var itemDetailsFactory = function($http) {

    var _ = require("underscore");

    var items = [];

    var refresh = function(callback, err) {
        $http.get("/api/data").success(function(data) {
            items = data;
            callback();
        }).error(err);
    };

    var initItems = function(callback, err) {
        if (items.length === 0) {
            refresh(callback, err);
        } else {
            if (callback) callback();
        }
    };

    var getItems = function(callback) {
        var cb = function() {
            callback(items);
        }

        var err = function() {
            callback(undefined);
        }

        initItems(cb, err);
    };

    var getItemById = function(id, callback) {
        var cb = function() {
            var result = _.find(items, (i) => i.id === id);
            callback(result);
        };

        var err = function() {
            callback(undefined);
        }

        initItems(cb, err);
    };

    return {
        getItems: getItems,
        getItemById: getItemById,
        refresh: refresh
    };
};

export function register (app) {

    app.factory("ItemDetailsService", [ "$http", itemDetailsFactory ]);

}

