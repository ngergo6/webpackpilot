﻿import * as itemDetailsService from "./ItemDetailsService";


export function register(app) {

    itemDetailsService.register(app);

};