﻿var gulp = require("gulp"),
    gutil = require("gulp-util"),
    webpack = require("gulp-webpack"),
    inject = require("gulp-inject"),
    del = require("del"),
    path = require("path"),
    plumber = require("gulp-plumber"),
    minifyCss = require("gulp-minify-css"),
    concatCss = require("gulp-concat-css"),
    uglify = require("gulp-uglify"),
    babel = require("gulp-babel");

var config = require("./Gulpfile.config"); // load the configuration

/**
 * JS processing
 */

// clean old js files
gulp.task("js:clean", [], function (callback) {
    del(config.js.dest + "/*.js", callback); // clean up our dest folder
});

// pack the js files up
gulp.task("js:build", ["js:clean"], function () {
    return gulp
        .src(config.js.sources) // read the source files
        .pipe(plumber()) // error management
        .pipe(babel()) // translate ES6 to ES5
        .pipe(webpack(config.webpackOptions)) // pack our modules
        .pipe(config.env.dev ? gutil.noop() : uglify()) // minify outside of development environment
        .pipe(gulp.dest(config.js.dest)); // write out the result to the dest folder
});

/**
 * CSS
 */

// clean old css files
gulp.task("css:clean", [], function (callback) {
    del(config.css.dest + "/*.css", callback); // clean out the dest folder
});

// build css
gulp.task("css:build", ["css:clean"], function () {
    return gulp
        .src(config.css.sources) // read the source files
        .pipe(plumber()) // error management
        .pipe(config.env.dev ? gutil.noop() : minifyCss({})) // minify outside of development environment
        .pipe(config.env.dev ? gutil.noop() : concatCss("bundle.css")) // concat our css together outside of development environment
        .pipe(gulp.dest(config.css.dest)); // write out the result to the dest folder
});

/**
 * Common asset management
 */

// inject assets into layout
gulp.task("inject", ["css:build", "js:build"], function () {
    var sources = gulp // load the source files
        .src([
            config.css.dest + "/**/*.css",
            config.js.dest + "/**/*.js"
        ], { read: false }); // but only the list, not the content

    return gulp
        .src(path.join(config.layout.dir, config.layout.file)) // read the layout file
        .pipe(plumber()) // error management
        .pipe(inject(sources, { relative: true })) // inject the css and js into our layout
        .pipe(gulp.dest(config.layout.dir)); // write the layout back
});

/**
 * 'public' commands
 * use --type=dev to turn off minification
 */

// use this task to build the project
gulp.task("build", ["inject"], function () {

});

// watch files during development and build on changes
gulp.task("build:watch", [], function () {
    gulp.watch([config.appDir + "/**/*"], ["inject"]);
});