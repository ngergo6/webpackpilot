﻿var path = require("path"),
    gutil = require("gulp-util"),
    webpack = require("webpack");

var config = {
    // webpack options
    webpackOptions: {
        output: {
            publicPath: "http://localhost:58319/Scripts/dist/js/"
        },
        module: {
            loaders: [
                { test: /\.js$/, exclude: [/node_modules/, /bower_components/], loader: "babel-loader" }
            ]
        }
    },
    // js specific config
    js: {
        // list of js files for webpack to process
        sources: [
            "./App/Client.js"
        ],
        // dest location of the js files
        dest: "./dist/js"
    },
    // css specific config
    css: {
        // list of css files
        sources: [
            "../Content/**/*.css",
            "./bower_components/bootstrap/dist/css/bootstrap.min.css"
        ],
        // dest location of the css files
        dest: "./dist/css"
    },
    // layout paths
    layout: {
        // _layout filename
        file: "_Layout.cshtml",
        // path of the _layout file
        dir: "../Views/Shared"
    },
    // path of the app dir
    appDir: "./App",
    env: {
        prod: true,
        test: false,
        dev: false
    }
};

// set up env
switch (gutil.env.type) {
    case "test":
        config.env.test = true;
        config.env.prod = false;
        config.env.dev = false;
        break;
    case "dev":
        config.env.test = false;
        config.env.prod = false;
        config.env.dev = true;
        break;
    case "prod":
    case undefined:
    default:
        config.env.test = false;
        config.env.prod = true;
        config.env.dev = false;
        break;
}

module.exports = config;