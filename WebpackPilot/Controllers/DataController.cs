﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebpackPilot.Controllers
{
    public class DataController : ApiController
    {
        [HttpGet]
        [Route("api/data")]
        public IHttpActionResult GetData()
        {
            var descs = new[]
            {
                "Desk", "Table", "Window", "Door", "Brick", "Haystack", "Computer", "Mouse", "Cat", "Watch", "Medicine"
            };

            var data = Enumerable.Range(1, 50).Select(i => new
            {
                id = i,
                name = string.Format("Item #{0}", i),
                description = string.Format("This is a very nice {0}", descs[i % descs.Length])
            }).ToList();

            return Ok(data);
        }
    }
}
